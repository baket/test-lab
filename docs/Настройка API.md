1.Убедитесь, что все зависимости из requirements.txt установлены
    pip install -r requirements.txt
2.Добавьте в файл settings_local.py (путь с корневой директории: /source/main/setting_local.py) следующее:
    ```
        CORS_ORIGIN_WHITELIST = (
            'http://localhost:8080'
        )
    ```
Обратите внимание, что 'http://localhost:8080' - это адрес сервера, на котором работает клиентская часть приложения

Документация API в файле Test_API.md