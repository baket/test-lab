1. Дополнительная информация - получение списка

    URL : "http://localhost:8000/api/helper-info/"
    
    Method : "GET"

    Ответ:
    
        Возрвращается массив из объектов. Каждый объект состоит из следующих полей:
        
            1) "id" - идентификационный номер

            2) "title" - название
            
            3) "content" - контент

2. Дополнительная информация - добавление

    URL : "http://localhost:8000/api/helper-info/"
    
    Method : "POST"

    Body:
    
        Должны быть предоставлены нижеперечисленные поля:
        
            1) "title" - название (required)
             
            2) "content" - контент (required)
    
    Ответ:
        
        Возвращается созданный объект, состоящий из следующих полей:
        
            1) "id" - идентификационный номер

            2) "title" - название
            
            3) "content" - контент


3. Дополнительная информация - получение по ID

    URL : "http://localhost:8000/api/helper-info/<int:id>"
    
    Method : "GET"

    Ответ:
    
        В ответе предоставляются следующие поля:
        
            1) "id" - идентификационный номер

            2) "title" - название
            
            3) "content" - контент

4. Дополнительная информация - редактирование по ID

    URL : "http://localhost:8000/api/helper-info/<int:id>"
    
    Method : "PUT"

    Body:
    
        Должны быть предоставлены нижеперечисленные поля:
        
            1) "title" - нов. название (required)
            
            2) "content" - нов. контент (required)

5. Дополнительная информация - удаление по ID

    URL : "http://localhost:8000/api/helper-info/<int:id>"
    
    Method : "DELETE"

    Ответ : (нет ответа)