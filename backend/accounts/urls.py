from django.urls import path, include

from accounts.views import ConfirmEmailView

urlpatterns=[
    path('', include('rest_auth.urls')),
    path('registration/', include('rest_auth.registration.urls')),
    path(r'registration/account-confirm-email/(?P<key>[-:\w]+)/$', ConfirmEmailView.as_view(), name='account_confirm_email'),
    ]