from ckeditor_uploader.fields import RichTextUploadingField
from django.core.validators import ValidationError
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):
	title = models.CharField(max_length=100, default='')
	slug = models.SlugField(max_length=50, default='', null=False)
	parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
	test_count = models.IntegerField(verbose_name='Количество тестов', default=0)

	def __str__(self):
		return self.title

	class MPTTMeta:
		order_insertion_by = ['title']

	class Meta:
		unique_together = ('slug', 'parent',)

	def get_absolute_url(self):
		return '/'.join([getattr(node, 'slug') for node in self.get_ancestors(include_self=True)])


class HelperInformation(models.Model):
	title = models.CharField(max_length=100, blank=False, null=False)
	content = RichTextUploadingField('Content', default='')

	def __str__(self):
		return self.title


class Test(models.Model):
	title = models.CharField(max_length=100, default='', blank=False)
	duration = models.IntegerField(default=30, blank=False)
	helper_information = models.ForeignKey(HelperInformation, on_delete=models.CASCADE, blank=True, null=True)
	category = TreeForeignKey(Category, on_delete=models.CASCADE, blank=False, null=False, related_name='tests')


	def __str__(self):
		return self.title

	def clean(self, *args, **kwargs):
		if not self.category.is_leaf_node():
			raise ValidationError('Select Leaf Category')

class Question(models.Model):
	QUESTION_CHOICES = (
		(1, 'Один ответ'),
		(2, 'Несколько ответов'),
		(3, 'Ответ в виде строки')
	)
	text = RichTextUploadingField(verbose_name="Вопрос")
	extra_info = models.ForeignKey(HelperInformation, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Дополнительная информация")
	test = models.ForeignKey(Test, on_delete=models.CASCADE, blank=False, null=False, related_name="questions", verbose_name="Тест")
	type = models.IntegerField(choices=QUESTION_CHOICES, default=0, verbose_name="Тип")

	def __str__(self):
		return "{0}. №{1}".format(self.test, self.id)

	# def clean(self, *args, **kwargs):
	# 	if not self.answers.count():
	# 		raise ValidationError('Добавьте ответ/ответы.')
	#
	# 	correct_answers = self.answers.filter(is_correct=True)
	# 	if self.type == 1 and correct_answers.count() != 1:
	# 		raise ValidationError('Отметьте только один правильный ответ.')

class Answer(models.Model):
	text = RichTextUploadingField("Ответ",null=False)
	is_correct = models.BooleanField("Правильный ответ", help_text="Не нужно отмечать если ответ в виде строки")
	question = models.ForeignKey(Question, verbose_name="question", on_delete=models.CASCADE, related_name="answers")


