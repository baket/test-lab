from django.conf.urls import url
from django.urls import path,re_path

from webapp import views

urlpatterns = [

    path(r'tests', views.tests_list, name="tests-list"),
    path(r'tests/<int:pk>', views.test_details, name="test-details"),
    path(r'category/<int:pk>/tests', views.tests_by_category_id, name="tests-by-category-id"),
#     re_path(r'category/(?P<path>.*)/tests', views.tests_by_category_path, name="tests-by-category-path"),

    path(r'category-main/', views.CategoryMain.as_view(), name="category-main"),
    re_path(r'category/(?P<path>.*)', views.CategoryByPath.as_view(), name="category-by-path"),
    path(r'category/<int:pk>', views.CategoryDetails.as_view(), name="category-details"),
    path(r'category-tree/', views.CategoryTree.as_view(), name="category-tree"),

    path(r'helper-info', views.helper_info_list, name="helper-info"),
    path(r'helper-info/<int:pk>', views.helper_info_details, name="helper-info-detail"),

    path(r'questions', views.questions_list, name="questions-list"),
    path(r'questions/<int:pk>', views.questions_detail, name="question"),
    path(r'tests/<int:pk>/questions', views.questions_by_test_id, name="questions-by-test"),
    path(r'tests/<int:test_id>/helper-info', views.helper_info_by_test_id, name='helper-info-by-test-id') # get helper-info by test_id
]
