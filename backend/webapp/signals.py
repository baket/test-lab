from webapp.models import Test, Category
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver


@receiver(post_save, sender=Test)
def increment_test_count(sender, instance,created, **kwargs):
    if (kwargs.get('created', True) and not kwargs.get('raw', False)):
        if created:
            category = instance.category
            while category:
                category.test_count += 1
                category.save()
                category = category.parent


@receiver(post_delete, sender=Test)
def decrement_test_count(sender, instance, **kwargs):
    category = instance.category
    while category:
        category.test_count -= 1
        category.save()
        category = category.parent
