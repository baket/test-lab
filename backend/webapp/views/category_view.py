from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Q
from django.db.models import CharField, Value
from webapp.models import Category
from webapp.serializer import ( CategoryRecursiveSerializer, CategorySerializer,
        CategoryWithTestsSerializer,MainCategorySerializer)
from .utils import get_category_by_path


class CategoryTree(APIView):
	def get(self, request, *args, **kwargs) -> Response:
		categories = Category.objects.root_nodes()
		serializer = CategoryRecursiveSerializer(categories, many=True)
		return Response(serializer.data)


class CategoryMain(APIView):
	def get(self, request, *args, **kwargs) -> Response:
		categories = Category.objects.root_nodes()
		serializer = MainCategorySerializer(categories, many=True)
		return Response(serializer.data)


class CategoryByPath(APIView):
	def get(self, request, *args, **kwargs) -> Response:
		try:
			parent_category = get_category_by_path(kwargs.get('path'))
		except Category.DoesNotExist:
			return Response(status=status.HTTP_404_NOT_FOUND)
		include_tests = request.GET.get('tests') == 'true'

		if include_tests:
			categories = Category.objects.filter((Q(id=parent_category.id)|Q(parent=parent_category))&Q(children__isnull=True)&Q(test_count__gte=1))
			serializer = CategoryWithTestsSerializer(categories, many=True)
		else:
			categories = Category.objects.filter(parent=parent_category, test_count__gte=1)
			serializer = CategorySerializer(categories, many=True)
		return Response(serializer.data)

class CategoryDetails(APIView):
	def __init__(self, *args, **kwargs):
		super().__init__(**kwargs)
		self.category = None

	def get(self, request, *args, **kwargs) -> Response:
		try:
			self.category = Category.objects.get(pk=kwargs['pk'])
		except Category.DoesNotExist:
			return Response(status=status.HTTP_404_NOT_FOUND)
		serializer = CategorySerializer(self.category)
		return Response(serializer.data)

	def put(self, request, *args, **kwargs) -> Response:
		try:
			self.category = Category.objects.get(pk=kwargs['pk'])
		except Category.DoesNotExist:
			return Response(status=status.HTTP_404_NOT_FOUND)

		serializer = CategorySerializer(self.category, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def delete(self, request, *args, **kwargs) -> Response:
		try:
			self.category = Category.objects.get(pk=kwargs['pk'])
		except Category.DoesNotExist:
			return Response(status=status.HTTP_404_NOT_FOUND)
		self.category.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

