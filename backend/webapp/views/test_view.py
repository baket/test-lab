from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from webapp.models import Category
from webapp.models import Test
from webapp.serializer import TestsSerializer, TestSerializer

from .utils import get_category_by_path


@api_view(['GET'])
def tests_list(request):
	"""
		Get all available tests in database
	"""
	tests = Test.objects.all()
	serializer = TestsSerializer(tests, many=True)
	return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def test_details(request, pk):
	"""
		Read, update or delete a test by id/pk.
	"""
	try:
		test = Test.objects.get(pk=pk)
	except Test.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)
	if request.method == 'GET':
		serializer = TestSerializer(test, context={'request': request})
		return Response(serializer.data)
	elif request.method == 'PUT':
		serializer = TestSerializer(test, data=request.data, context={'request': request})
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
	elif request.method == 'DELETE':
		test.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def tests_by_category_id(request, pk):
	"""
		Get tests by category id
	"""
	category = Category.objects.get(pk=pk)
	serializer = TestsSerializer(category.tests, many=True)
	return Response(serializer.data)


@api_view(['GET', 'POST'])
def tests_by_category_path(request, path):
	"""
		Retrieve test list, create new test by category path
	"""
	try:
		category = get_category_by_path(path)
	except Category.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		tests = category.tests
		serializer = TestsSerializer(tests, context={'request', request}, many=True)
		return Response(serializer.data)
	elif request.method == 'POST':
		serializer = TestsSerializer(data=request.data)
		if serializer.is_valid():
			serializer.validated_data['category'] = category
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
