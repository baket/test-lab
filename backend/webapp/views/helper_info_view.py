from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from webapp.models import HelperInformation
from webapp.serializer import HelperInformationSerializer


@api_view(['GET', 'POST'])
def helper_info_list(request):
	"""
	Retrieve test list, insert test by category path
	"""
	if request.method == 'GET':
		info_samples = HelperInformation.objects.all()
		serializer = HelperInformationSerializer(info_samples, context={'request', request}, many=True)
		return Response(serializer.data)
	elif request.method == 'POST':
		serializer = HelperInformationSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'PUT', 'DELETE'])
def helper_info_details(request, pk):
	"""
	Retrieve, update or delete a helper information by id/pk.
	"""
	try:
		# info_sample = HelperInformation.objects.get(pk=pk)
		info_sample = HelperInformation.objects.get(pk=pk)
	except HelperInformation.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)
	if request.method == 'GET':
		serializer = HelperInformationSerializer(info_sample, context={'request': request})
		return Response(serializer.data)
	elif request.method == 'PUT':
		serializer = HelperInformationSerializer(info_sample, data=request.data, context={'request': request})
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
	elif request.method == 'DELETE':
		info_sample.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def helper_info_by_test_id(request, test_id):
	"""
	Retrieve, update or delete a helper information by TEST id/pk.
	"""
	try:
		info_sample = HelperInformation.objects.get(test__id=test_id)
	except:
		return Response(status=status.HTTP_404_NOT_FOUND)

	serializer = HelperInformationSerializer(info_sample, context={'request': request})
	return Response(serializer.data)
