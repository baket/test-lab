from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from webapp.models import Question
from webapp.models import Test
from webapp.serializer import (QuestionSerializer)


@api_view(['GET'])
def questions_list(request):
	"""
	Retrieve questions list - all of them
	"""
	questions = Question.objects.all()
	serializer = QuestionSerializer(questions, many=True)
	return Response(serializer.data)
	

@api_view(['GET','PUT','DELETE'])
def questions_detail(request, pk):

	try:
		question = Question.objects.get(pk=pk)
	except Question.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)
	if request.method == 'GET':
		serializer = QuestionSerializer(question, context={'request', request})
		return Response(serializer.data)
	elif request.method == 'PUT':
		serializer = QuestionSerializer(question, data=request.data, context={'request', request})
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
	elif request.method == 'DELETE':
		question.delete()
		return Response(status=status.HTTP_202_ACCEPTED)


@api_view(['GET', 'POST'])
def questions_by_test_id(request, pk):
	"""
		Retrieve questions by test id
	"""
	try:
		test = Test.objects.get(pk=pk)
	except Test.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = QuestionSerializer(test.questions, context={"request": request}, many=True)
		return Response(serializer.data)
	elif request.method == 'POST':
		serializer = QuestionSerializer(data=request.data)
		if serializer.is_valid():
			print(serializer.validated_data)
			serializer.validated_data['test'] = test
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

