from webapp.models import Category


def get_category_by_path(path):
	path = path.rstrip('/')
	slugs = path.split('/')
	last_slug = slugs[-1]
	samples = Category.objects.filter(slug=last_slug)
	for sample in samples:
		if sample.get_absolute_url() == path.rstrip('/'):
			return sample
	raise Category.DoesNotExist
