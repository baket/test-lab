from django.contrib import admin
import nested_inline.admin as nested_admin

from .models import Category, Test, HelperInformation, Question, Answer, Test
from mptt.admin import MPTTModelAdmin,TreeRelatedFieldListFilter


class AnswersInline(nested_admin.NestedTabularInline):
	model = Answer
	extra = 0
	fk_name = 'question'


class QuestionsInline(nested_admin.NestedStackedInline):
	model = Question
	extra = 1
	fk_name = 'test'
	inlines = [AnswersInline]


class TestAdmin(nested_admin.NestedModelAdmin):
	model = Test
	inlines = [QuestionsInline]
	list_filter = (
		('category', TreeRelatedFieldListFilter),
	)


class TestsInline(nested_admin.NestedStackedInline):
	model = Test
	fk_name = 'category'
	extra = 1

class CustomCategoryAdmin(MPTTModelAdmin):
	mptt_level_indent = 35
	fields = ('title', 'slug', 'parent')
	inlines = [TestsInline]

class QuestionAdmin(nested_admin.NestedModelAdmin):
	model = Question
	inlines = [AnswersInline]


admin.site.register(Test, TestAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Category,CustomCategoryAdmin)
admin.site.register(HelperInformation)
