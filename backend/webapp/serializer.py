from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from rest_framework.fields import CharField
from django.db.models import Q
from webapp.models import Category, Question, Test, Answer, HelperInformation


class CategoryRecursiveSerializer(serializers.ModelSerializer):
	children = RecursiveField(many=True)

	class Meta:
		model = Category
		fields = ('id', 'title', 'slug', 'children', 'test_count')


class CategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = Category
		fields = ('id', 'title', 'slug', 'parent', 'test_count')

class MainCategorySerializer(serializers.ModelSerializer):
    child_slug = serializers.SerializerMethodField('get_child_slug')
    class Meta:
        model = Category
        fields = ('id', 'title', 'slug', 'child_slug')
    def get_child_slug(self, obj):
        child = Category.objects.filter((Q(parent=obj))&Q(children__isnull=True)&Q(test_count__gte=1)).first()
        if child:
            return child.slug
        else:
            return "null"


class HelperInformationSerializer(serializers.ModelSerializer):
	class Meta:
		model = HelperInformation
		fields = ('id', 'title', 'content')


class AnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		fields = ('id', 'text')


class QuestionSerializer(serializers.ModelSerializer):
	answers = AnswerSerializer(many=True, read_only=True)

	class Meta:
		model = Question
		fields = ('id','type', 'extra_info', 'text', 'answers', 'test')



class TestSerializer(serializers.ModelSerializer):
	questions = QuestionSerializer(many=True, read_only=True)
	helper_information = HelperInformationSerializer(read_only=True)

	class Meta:
		model = Test
		fields = ('id', 'title', 'duration', 'helper_information', 'questions', 'category')

class TestsSerializer(serializers.ModelSerializer):
	questions_count = serializers.SerializerMethodField('get_questions_count')
	class Meta:
		model = Test
		fields = ('id', 'title', 'duration', 'questions_count')
	def get_questions_count(self, obj):
		return obj.questions.count()
class CategoryWithTestsSerializer(serializers.ModelSerializer):
    tests = TestsSerializer(many=True, read_only=True)
    class Meta:
        model = Category
        fields = ('title', 'tests')
