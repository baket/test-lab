import json
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from webapp.models import Category
from webapp.serializer import CategorySerializer, CategoryRecursiveSerializer

client = Client()


class CategoryTest(TestCase):
    def setUp(self):
        self.ort = Category.objects.create(
            title='ОРТ', slug='ort', parent=None)
        self.nct = Category.objects.create(
            title='nct', slug='ort', parent=None)
        self.main = Category.objects.create(
            title='Основной', slug='main', parent=self.ort)
        Category.objects.create(
            title='Математика', slug='math', parent=self.main)
        Category.objects.create(
            title='Математика', slug='math', parent=self.ort)
        Category.objects.create(
            title='Математика', slug='math', parent=self.nct)

    def test_get_categories_tree(self):
        response = client.get(reverse('category-tree'))
        category_tree = Category.objects.root_nodes()
        serializer = CategoryRecursiveSerializer(category_tree, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_main_categories(self):
        response = client.get(reverse('category-main'))
        categories = Category.objects.root_nodes()
        serializer = CategorySerializer(categories, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_category_by_path(self):
        response = client.get(reverse('category-by-path', kwargs={'path': 'ort/'}))
        category = Category.objects.get(pk=self.ort.pk).get_children()
        serializer = CategoryRecursiveSerializer(category, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_category_by_path_invalid(self):
        response = client.get(reverse('category-by-path', kwargs={'path': 'not-existed-category/'}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_category_by_path(self):
        payload = {
            'title': 'Аналогия',
            'slug': 'analogiya',
            'parent': self.main.pk
        }
        response = client.post(
            reverse('category-by-path', kwargs={'path': 'ort/main/'}),
            data=json.dumps(payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_category_by_path_invalid(self):
        payload = {
            'title': 'Аналогия',
            'slug': 'analogiya',
            'parent': self.main.pk
        }
        response = client.post(
            reverse('category-by-path', kwargs={'path': 'ort/abbye'}),
            data=json.dumps(payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_category_root(self):
        payload = {
            'title': 'Some Main Category',
            'slug': 'new_one',
            'parent': None
        }
        response = client.post(
            reverse('category-main'),
            data=json.dumps(payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def get_category_by_id_valid(self):
        response = client.get(reverse('category', kwargs={'pk': self.ort.pk}))
        category = Category.objects.get(pk=self.ort.pk)
        serializer = CategorySerializer(category)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def get_category_by_id_invalid(self):
        response = client.get(reverse('category', kwargs={'pk': 999}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def update_category_by_id_valid(self):
        payload = {
            'title': 'Общереспубликанское тестирование',
            'slug': 'ort',
            'parent': None,
        }
        response = client.put(
            reverse('category', kwargs={'pk': self.ort.pk}),
            data=json.dumps(payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def update_category_by_id_invalid(self):
        payload = {
            'title': 'ORT - without parent specified',
            'slug': 'none',
            'invalid_property': 999
        }
        response = client.put(
            reverse('category', kwargs={'pk': self.ort.pk}),
            data=json.dumps(payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def delete_category_by_id_valid(self):
        response = client.delete(
            reverse('category', kwargs={'pk': self.ort.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def delete_category_by_id_invalid(self):
        response = client.delete(
            reverse('category', kwargs={'pk': 999})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)