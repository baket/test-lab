DROP DATABASE IF EXISTS `testlab_db`;
CREATE DATABASE `testlab_db`
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

GRANT ALL PRIVILEGES ON testlab_db.* TO 'testlab'@'localhost' IDENTIFIED BY 'baket';
GRANT ALL PRIVILEGES ON test_testlab_db.* TO 'testlab'@'localhost' IDENTIFIED BY 'baket';

FLUSH PRIVILEGES;