import Vue from 'vue';
import Router from 'vue-router';

import Home from "./views/Home";
import TestsOverview from "./views/TestsOverview";
import TestDetail from "./views/TestInfo";
import Quiz from "./components/quiz/Quiz";
import Login from "./views/Login";
import Register from "./views/Register";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home-page',
      component: Home,
    },
    {
      path: '/home',
      redirect: '/'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/category/:category/:subcategory',
      component: TestsOverview,
    },
    {
      path: '/tests/:test_id/info',
      component: TestDetail
    },
    {
      path: '/tests/:test_id',
      component: Quiz,
    },
  ]
});
