import axios from "axios"

const BASE_URL = process.env.VUE_APP_API_BASEURL
export default {
	state: {
	},
	getters: {
	},
	actions: {
		async getHelperInfoByTestId({commit}, test_id) {
			const info = await axios
				.get(`${BASE_URL}/tests/${test_id}/helper-info`)
				.then(r => r.data)
				.catch(e => {
          commit('SET_ERROR', e);
					throw e;
				});
      return info;
		},
		async getQuestionsByTestId({commit}, test_id) {
			const questions = await axios
				.get(`${BASE_URL}/tests/${test_id}/questions`)
				.then(r => r.data)
				.catch(e => {
					commit('SET_ERROR', e);
					throw e;
				});

			return questions;
		}
	},
	mutations: {
	}
}
