import axios from "axios"

const BASE_URL = process.env.VUE_APP_API_BASEURL
export default {
	state: {
		main_categories: [],
		categories: [],
		subcategories: []
	},
	getters: {
		categories: s => s.categories,
		subcategories: s => s.subcategories,
		main_categories: s => s.main_categories
	},
	actions: {
		async fetchMainCategories({commit}) {
			await axios
				.get(`${BASE_URL}/category-main/`)
				.then(r => r.data)
				.then(data => {
					commit('SET_MAIN_CATEGORIES', data);
				})
				.catch(e => {
					commit('SET_MAIN_CATEGORIES', []);
					commit('SET_ERROR', e);
					throw e;
				});
		},
		async fetchCategories({commit}, category) {
			await axios
				.get(`${BASE_URL}/category/${category}`)
				.then(r => r.data)
				.then(data => {
					commit('SET_CATEGORIES', data);
				})
				.catch(e => {
					commit('SET_CATEGORIES', []);
					commit('SET_ERROR', e);
					throw e;
				});
		},
		async fetchSubcategories({commit}, {category, subcategory}) {
				await axios
					.get(`${BASE_URL}/category/${category}/${subcategory}?tests=true`)
					.then(r => r.data)
					.then(data => {
						commit('SET_SUBCATEGORIES', data);
					})
					.catch(e => {
						commit('SET_SUBCATEGORIES', []);
						commit('SET_ERROR', e);
						throw e;
					});
		}
	},
	mutations: {
		SET_CATEGORIES(state, s) {
			state.categories = [...s];
		},
		SET_SUBCATEGORIES(state, s) {
			state.subcategories = [...s];
		},
		SET_MAIN_CATEGORIES(state, s) {
			state.main_categories = [...s];
		}
	}
}