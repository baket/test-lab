import Vue from 'vue'
import Vuex  from 'vuex'
import category from './category'
import test from './test'

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        error: null,
    },
    getters: {
        error: s => s.error,
    },
    actions: {

    },
    mutations: {
        SET_ERROR(state, error) {
            state.error = error
            console.error("!!! : ",error);
        },
        CLEAR_ERROR(state) {
            state.error = null
        },
    },
    modules: {
        category,
        test
    }
})
