import Vue from "vue"
import axios from "axios"
import VueAxios from "vue-axios";
import store from "./store"
import App from "./App"
import router from "./router"
import DefaultUser from "./layouts/Default"
import './assets/styles/main.css'


Vue.use(VueAxios, axios);
Vue.component('default-layout', DefaultUser);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
  store
}).$mount("#app");
